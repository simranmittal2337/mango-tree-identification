import java.util.*;
public class MangoTree {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int rows =  sc.nextInt();
        int columns = sc.nextInt();
        int treeNumber = sc.nextInt();
        if(treeNumber%rows != 0){
            int treeRow = (int)treeNumber/rows + 1;
            int treeColumn = treeNumber%rows;
            if(treeRow == 1 || treeColumn == 1){
                System.out.println("Yes");
            }
            else{
                System.out.println("No");
            }
        }
        else{
            System.out.println("Yes");
        }
    }
}
